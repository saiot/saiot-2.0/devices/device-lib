#ifndef SAIOTDATA_H
#define SAIOTDATA_H

#include <Arduino.h>
#include <ArduinoJson.h>
#include <vector>

#define CALLBACK_ACTUATOR_TYPE std::function<std::vector<String>(std::vector<String> value)>
#define CALLBACK_SENSOR_TYPE std::function<std::vector<String>()>
#define CALLBACK_CONDITION_TYPE std::function<bool()>

enum class Evtype {event, alarm, normalization,other};

class sensorData
{
    private:

        String id;
        String value;
        String date;
        
    public:

        sensorData(String id,
                   String value,
                   String date);
        ~sensorData();

        String getJson();
        String getTopic(String idDispositivo);

        String saveData();
};

class eventData
{
    private:
        String id;
        String description;
        String date;
        int wifiSignalLevel;
        int bateryLevel;
        Evtype criticality;

    public:
        eventData(String id,
                  String description,
                  String date,
                  int wifiSignalLevel,
                  int bateryLevel,
                  Evtype criticality);

        String getJson();
        String getNotifyJson();

        String getTopic(String idDispositivo);
        String getNotifyTopic(String idDispositivo);

        Evtype getType();

        String saveData();

};

#endif