#include <Arduino.h>

#include "Saiot.h"

#define LED_PIN 2

String deviceName = "LED";
String deviceClass = "lampada";
String deviceDescription = "LED do ESP8266";

Saiot saiot(deviceName, deviceClass, deviceDescription);

// Callback do atuador
std::vector<String> ledSwitchCallback(std::vector<String> value);

void setup()
{
  Serial.begin(115200);

  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, HIGH);

  Actuator *led = new Actuator("LedSwitch", "LED", "switch", {"false"}, ledSwitchCallback);
  saiot.addActuator(*led);
  saiot.start();
}

void loop()
{
  saiot.loop();
}

std::vector<String> ledSwitchCallback(std::vector<String> value)
{
  String ledState = value[0];

  Serial.println("Recebido valor no atuador ledSwitch: " + ledState);

  if (ledState.compareTo("true") == 0)
  {
    digitalWrite(LED_PIN, LOW);
    return value;
  }
  else
  {
    digitalWrite(LED_PIN, HIGH);
    return value;
  }
}
