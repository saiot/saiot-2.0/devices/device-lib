#include "Actuator.h"

Actuator::Actuator(String nid, 
                   String nname,
                   String ntype, 
                   std::vector<String> nvalue,
                   CALLBACK_ACTUATOR_TYPE ncallback): id(nid),
                                                      name(nname),
                                                      type(ntype),
                                                      value(nvalue),
                                                      callback(ncallback)
{}

Actuator::~Actuator() {}

String Actuator::json()
{
    String json = "";

    json += "\"";
    json += id;
    json += "\"";
    json += ":{";

    json += "\"name\":\"";
    json += name;

    json += "\",\"type\":\"";
    json += type;

    json += "\",\"value\": [";
    
    for(unsigned int i =0; i<value.size(); i++)
    {
        json += value[i];
        if(i != value.size()-1){
        json += ",";
        }   
    }

    json += "]}";

    return json;
}

String Actuator::getId(){return this->id;}

std::vector<String> Actuator::callCallback(std::vector<String> nvalue){return callback(nvalue);}
