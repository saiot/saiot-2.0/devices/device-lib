#include "Sensor.h"

Sensor::Sensor(String nid, 
               String nname, 
               String ntype, 
               std::vector<String> value, 
               float timeout, 
               int deadband, 
               CALLBACK_SENSOR_TYPE callback): id(nid),
                                               name(nname),
                                               type(ntype),
                                               value(value),
                                               timeout(timeout),
                                               deadband(deadband),
                                               callback(callback)
{}

Sensor::~Sensor() {}

String Sensor::json()
{
    String json = "";

    json += "\"";
    json += id;
    json += "\"";
    json += ":{";

    json += "\"name\":\"";
    json += name;

    json += "\",\"type\":\"";
    json += type;

    json += "\",\"value\":";
    json += value[0];

    json += ",\"timeout\":";
    json += timeout;

    json += ",\"deadband\":";
    json += deadband;

    json += "}";

    return json;
}

String Sensor::getId(){return this->id;}

String Sensor::getType(){return this->type;}

void Sensor::setValue(std::vector<String> Value){this->value = Value;}

void Sensor::setValue(String Value){this->value.push_back(Value);}

std::vector<String> Sensor::getValue(){return value;}

sensorData Sensor::getSensorData(String creationTime)
{
    sensorData sensorData(id,value[0],creationTime);

    return sensorData;
}

void Sensor::setTimeout(float timeout){this->timeout = timeout;}

float Sensor::getTimeout(){return timeout;}

void Sensor::setDeadband(int deadband){this->deadband = deadband;}

int Sensor::getDeadband(){return deadband;}

unsigned long Sensor::getLastSendTime(){return lastSendTime;}

void Sensor::setLastSendTime(unsigned long lastSendTime){this->lastSendTime = lastSendTime;}

std::vector<String> Sensor::callCallback(){return callback();}
