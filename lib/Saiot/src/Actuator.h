#ifndef Actuator_H
#define Actuator_H

#include <Arduino.h>
#include <SaiotData.h>

class Actuator
{
private:
    String id;
    String name;
    String type;
    std::vector<String> value;
    CALLBACK_ACTUATOR_TYPE callback;

public:
    Actuator(String nid, 
             String nname,
             String ntype,
             std::vector<String> nvalue,
             CALLBACK_ACTUATOR_TYPE ncallback);
    ~Actuator();
    String json();
    String getId();
    std::vector<String> callCallback(std::vector<String> value);
};

#endif