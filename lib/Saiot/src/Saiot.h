#ifndef SAIOT_H
#define SAIOT_H

#include <Arduino.h>
#include <WiFiManager.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>

#if defined(ESP8266)
    #include <LittleFS.h>
#elif defined(ESP32)
    #include "SPIFFS.h"
    #include "FS.h"
#else
#endif

#include <ArduinoOTA.h>

#include "SaiotData.h"

#include "Actuator.h"
#include "Sensor.h"
#include "Event.h"

#define MAX_BUFFER_SIZE 70

class Saiot
{
private:
    WiFiClient espClient;
    PubSubClient pubSubClient;
    WiFiManager wifiManager;

    String broker_url = "dev.saiot2.ect.ufrn.br";
    int broker_port = 8000;

    unsigned int MaxBufferSize = MAX_BUFFER_SIZE;
    long tz = -3;
    byte dst = 0;
    wl_status_t lastStatus = WL_DISCONNECTED;

    bool hasValidId = false;
    bool hasValidId2 = false;
    bool firsttime = false;
    bool notime = true;
    int dataSaved = 0;

    String id;
    String email;
    String password;
    String deviceName;
    String deviceClass;
    String deviceDescription;

    String SSIDName = "SaiotDevice";

    std::vector<Actuator*> actuators;

    std::vector<Sensor*> sensors;

    std::vector<Event*> events;

    std::vector<sensorData>::iterator itSbuffer;
    std::vector<sensorData> sBuffer;

    std::vector<eventData>::iterator itEbuffer;
    std::vector<eventData> eBuffer;

    boolean logged;

    char emailr[50];
    char senhar[20];
    
    WiFiManagerParameter custom_senha{"SENHA", "senha", senhar, 20};
    WiFiManagerParameter custom_email{"EMAIL","email", emailr, 50};

    boolean enableOTAStart = true;
    bool otastart = false;

    void connectWifi();
    bool saveAccount();
    bool checkAccount();
    void connectMqtt();
    void reconnect();

    String json();
    void subscribe(const char *topic);
    boolean sendMessage(const char *topic, const char *payload);
    void mqttCallback(char *topic, byte *payload, unsigned int length);

    void eventProcess();
    void sensorProcess();
    void queueProcess();

    bool saveBuffer();
    bool readBuffer();

    void getFormattedtime(char* formTime);
    
public:
    Saiot(String ndeviceName,
          String ndeviceClass,
          String ndeviceDescription);
    ~Saiot();

    void addActuator(Actuator &newActuator);
    void addSensor(Sensor &newSensor);
    void addEvent(Event &newEvent);

    void setSSID(String nSSID);
    void setMaxbuffersize(int nmbuffer);
    void setBrokerUrl(String nURL);
    void setBrokerUrl(String nURL, int nport);
    void setCredentials(String email, String Password); 
    

    void start();
    boolean loop();

    void actState(int actnumb, String newState);
    
    void enableOTA();
    void handleOTA();
    void disableOTA();
};

#endif
