#ifndef EVENT_H
#define EVENT_H

#include <Arduino.h>
#include <LittleFS.h>
#include <SaiotData.h>

class Event
{
private:
    String id;
    String description;
    int wifiSignalLevel;
    int bateryLevel;
    bool notify = false;
    Evtype criticality;

    CALLBACK_CONDITION_TYPE condition;
    CALLBACK_SENSOR_TYPE callback;

public:
    Event(String nid, 
          String ndescription,
          int wifiSignalLevel, 
          int bateryLevel,
          Evtype criticality,
          CALLBACK_CONDITION_TYPE condition,
          CALLBACK_SENSOR_TYPE callback);
         
    ~Event();
  
    String getId();
    String getDescription();
    int getWifiSignalLevel();
    int getBateryLevel();

    String callCallback();
    bool callCondition();

    eventData getEventData(String creationTime);

    bool getNotify();
    void setNotify(bool notify);
};

#endif