#include "Event.h"
#include <ArduinoJson.h>


Event::Event(String nid, 
             String ndescription, 
             int wifiSignalLevel, 
             int bateryLevel,
             Evtype criticality,
             CALLBACK_CONDITION_TYPE condition,
             CALLBACK_SENSOR_TYPE callback): id(nid),
                                             description(ndescription),
                                             wifiSignalLevel(wifiSignalLevel),
                                             bateryLevel(bateryLevel),
                                             criticality(criticality),
                                             condition(condition),
                                             callback(callback)
{}
        
Event::~Event(){}

eventData Event::getEventData(String creationTime)
{
    eventData event(id,description,creationTime,wifiSignalLevel,
                    bateryLevel,criticality);
    return event;
}

String Event::getId(){return this->id;}
String Event::getDescription(){return this->description;}
int Event::getWifiSignalLevel(){return this->wifiSignalLevel;}
int Event::getBateryLevel(){return this->bateryLevel;}

String Event::callCallback(){return this->callback()[0];}

bool Event::callCondition(){return this->condition();}

bool Event::getNotify(){return this->notify;}

void Event::setNotify(bool notify){this->notify = notify;};
