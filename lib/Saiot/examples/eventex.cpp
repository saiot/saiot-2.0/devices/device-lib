#include <Arduino.h>

#include "Saiot.h"

String deviceName = "Temperatura";
String deviceClass = "temp";
String deviceDescription = "Temperatura com LM35";

Saiot saiot(deviceName, deviceClass, deviceDescription);

// Callback do sensor
std::vector<String> tempCallback();

void setup()
{
  Serial.begin(1152s00);

  Sensor *temp = new Sensor("temp", "temp", "number", {"0"}, 5, 20, tempCallback);
  saiot.addSensor(*temp);
  saiot.start();
}

void loop()
{
  saiot.loop();
}

std::vector<String> tempCallback()
{
  return 
}