#ifndef SENSOR_H
#define SENSOR_H

#include <Arduino.h>
#include <LittleFS.h>
#include <SaiotData.h>

class Sensor
{
private:
    String id;
    String name;
    String type;
    std::vector<String> value;
    float timeout;
    int deadband = 0;
    unsigned long lastSendTime;
    CALLBACK_SENSOR_TYPE callback;

public:
    Sensor(String id, 
           String name, 
           String type, 
           std::vector<String> nvalue, 
           float timeout, 
           int deadband, 
           CALLBACK_SENSOR_TYPE callback);
    ~Sensor();

    String json();

    String getId();
    String getType();

    void setValue(String Value);
    void setValue(std::vector<String> Value);

    std::vector<String> getValue();
    
    sensorData getSensorData(String creationTime);

    void setTimeout(float timeout);
    float getTimeout();

    void setDeadband(int deadband);
    int getDeadband();

    unsigned long getLastSendTime();
    void setLastSendTime(unsigned long lastSendTime);

    std::vector<String> callCallback();

};

#endif