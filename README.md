# Biblioteca de dispositivos Saiot 2.0

Saiot é uma plataforma de Internet of Things(IoT) ampla, ou seja, funciona para as mais diversas aplicações. Para simplificar a produção de dispositivos a biblioteca Saiot para dispositivos visa cuidar da conexão entre dispositivo e plataforma.

<a name="ancora"></a>
## Sumario
- [Ambiente de Desenvolvimento](#ancora1)
- [Métodos Principais](#ancora2)
- [Construíndo Um Exemplo](#ancora3)
- [Dispositivo em Funcionamento](#ancora4)
- [WishList e Bugs](#ancora5)
- [Contribuições e Agradecimentos](#ancora6)

<a id="ancora1"></a>
## Ambiente de Desenvolvimento

### Instalando o platformIO

O primeiro passo para que possamos desenvolver dispositivos para o SAIOT é um ambiente de desenvolvimento adequado. Temos duas possibilidades principais: 
- A utilização do [platformIO](https://platformio.org/) em conjunto com  o [Visual Studio Code](https://code.visualstudio.com/) (recomendado) ou o [Atom](https://atom.io/).
- A utilização do editor de código do [Arduino](https://www.arduino.cc/en/software).

Para o primeiro caso basta que no Visual Studio Code se instale, na aba de extensões, o platformIO. Como demonstrado na figura abaixo:

[![DLFwqx.png](https://iili.io/DLFwqx.png)](https://freeimage.host/br)


Uma vez que você tenha o ambiente pronto basta que se baixe o master desse repositório para que se comece o desenvolvimento de seu dispositivo.

Caso se deseje utilizar a IDE do arduino é necessario que se faça o download da pasta lib e se prepare para fazer a importação da biblioteca. (Não recomendado para usuarios menos experientes)




<a id="ancora2"></a>
## Métodos Principais

Os métodos são a principal forma de comunicação entre o usuário e a biblioteca. Os principais e suas funções estão dispostas logo abaixo.

```c++
Saiot saiot(deviceName, deviceClass, deviceDescription);
```

A criação da classe principal do dispositivo é essencial para o desenvolvimento de todas as atividades desenvolvidas pela biblioteca. Basta que se dê 3 nomes que serão refletidos na plataforma do SAIOT.

```c++
Actuator *[nameactuator] = new Actuator("id","name", "type", [valinicial]{"0"}, [nameactuator]Callback);

Sensor *[namesensor] = new Sensor("id","desc", "type", [valinicial]{"0"},[timeout]10,[deadband]20, [namesensor]Callback);

Event  *[nameevent] = new Event("id","desc",[Força Wifi]0,[Bateria]0,Evtype::alarm/normalization/event,[nameevent]Condition,[nameevent]Callback);

  saiot.addActuator(*[nameactuator]);
  saiot.addSensor(*[namesensor]);
  saiot.addEvent(*[nameevent]);
```

Com a criação dos objetos para sensores, atuadores e eventos e adicionando-os ao dispostivo saiot temos um dispostivo pronto para a conexão.

```c++
saiot.disableOTA();
```

Por padrão o OTA do dispositivo está ativado. Caso deseje economizar esse processamento basta utilizar esse método.

```c++
saiot.setSSID(String);
```

Decide qual nome o dispositivo terá quando puder ser conectado pelo wifi.

```c++
saiot.setMaxbuffersize(int);
```
Caso seja necessário que uma maior quantidade de dados seja enviada em um só pacote esse limite pode ser ajustado com esse método.

<a id="ancora3"></a>
## Construíndo Um Exemplo

Primeiro passo é baixar a master desse repositorio e abrir em sua IDE.

Uma vez dentro da pasta basta que se abra a pasta src e se faça edições na main.cpp. Aonde encontraremos algo como o código abaixo

```c++
#include "Saiot.h"

String deviceName = "nomedisp";
String deviceClass = "classedispositivo";
String deviceDescription = "descrição";

Saiot saiot(deviceName, deviceClass, deviceDescription);

// Callback do atuador
//std::vector<String [namedevice]Callback(std::vector<String value);

void setup()
{
  Serial.begin(115200);
//  Actuator/Sensor *[namedevice] = new Actuator/Sensor("id","name", "type", {}, [namedevice]Callback);
//  saiot.addActuator/Sensor(*led);
//  saiot.disableOTA(); caso não seja necessário
  saiot.start();
}

void loop()
{
  saiot.loop();
}

```
A partir daí para que tenhamos um dispositivo unido devemos lhe dar um nome, uma classe e uma breve descrição. 

Logo em seguida devemos, também, criar os sensores, atuadores e eventos que desejarmos juntamente com seus respectivos callbacks.

Abaixo iremos descrever cada um desses passos de forma mais detalhada.


### Nomeando Seu dispositivo

Para simplificação do codigo geramos as definições dos pinos de saida ou entrada logo no começo do código.

Em seguida mudamos as variaveis deviceName, deviceClass, deviceDescription!!!

```c++
#include "Saiot.h"

#define LED_PIN 2

String deviceName = "LED";
String deviceClass = "lampada";
String deviceDescription = "LED do ESP8266";

Saiot saiot(deviceName, deviceClass, deviceDescription);

```

Assim temos a primeira parte do código finalizada.

### Criando a função callback

A função callback é responsavel por definir o que um sensor ou atuador envia ao SAIOT. 

Para isso é preciso criar uma função que retorne um vector de strings!!!

Vamos exemplificar isso com uma função que faça um led piscar.


```c++

String ledSwitchCallback(String value[]);

void setup()
{
    (...)
}

void loop()
{
    (...)
}

std::vector<String> ledSwitchCallback(std::vector<String> value)
{
  String ledState = value[0];

  Serial.println("Recebido valor no atuador ledSwitch: " + ledState);

  if (ledState.compareTo("true") == 0)
  {
    digitalWrite(LED_PIN, LOW);
    return value;
  }
  else
  {
    digitalWrite(LED_PIN, HIGH);
    return value;
  }
}
```

### Criando seu sensor/atuador

Para criar nosso sensor/actuador devemos dar-lhe:

Um id que terá uso interno;

Um nome que será exibido no SAIOT;

Um dos tipos predefinidos no SAIOT;

    Para actuators:
    number: Que criar um actuador de numeros!!!
    switch: Que gera um botão transitorio entre dois estados!!!
    color: Que gera uma escala rgbx!!!
    slider: Que gera um deslizador de valores inteiros!!!
    custom: Que gera um actuator customizado!!!

** **

    Para Sensores:
    number: Cria um sensor que recebe valores numericos!!!
    on/off: Cria um sensor que recebe booleanos!!!
    gps: Cria um sensor que recebe Lat e Long e o transforma em um mapa!!!
** **
    Para Eventos:
    Evtype::alarm: Cria um alarme.
    Evtype::normalization: Cria a normalização de um alarme.
    Evtype::event: Cria um evento.

    A ativação do evento se da de acordo com a condição proposta numa função que retorna um booleano que deve ser passada na definição do evento.
    

E o Callback que criamos no passo anterios.

Um exemplo pode ser visto no código abaixo:

```c++

void setup()
{
  Serial.begin(115200);

  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, HIGH);

  Actuator *led = new Actuator("LedSwitch", "LED", "switch", {"false"}, ledSwitchCallback);
  saiot.addActuator(*led);
  saiot.start();
}

void loop()
{
  saiot.loop();
}


```

### Código Completo

Com tudo que fizemos até agora temos dispositivo capaz de conectar a plataforma SAIOT e ser controlado pelo sistema.

O código completo pode ser visto abaixo:
```c++
#include "Saiot.h"

#define LED_PIN 2

String deviceName = "LED";
String deviceClass = "lampada";
String deviceDescription = "LED do ESP8266";

Saiot saiot(deviceName, deviceClass, deviceDescription);

// Callback do atuador
std::vector<String> ledSwitchCallback(std::vector<String> value);

void setup()
{
  Serial.begin(115200);

  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, HIGH);

  Actuator *led = new Actuator("LedSwitch", "LED", "switch", {"false"}, ledSwitchCallback);
  saiot.addActuator(*led);
  saiot.start();
}

void loop()
{
  saiot.loop();
}

std::vector<String> ledSwitchCallback(std::vector<String> value)
{
  String ledState = value[0];

  Serial.println("Recebido valor no atuador ledSwitch: " + ledState);

  if (ledState.compareTo("true") == 0)
  {
    digitalWrite(LED_PIN, LOW);
    return value;
  }
  else
  {
    digitalWrite(LED_PIN, HIGH);
    return value;
  }
}
```

<a id="ancora4"></a>
## Dispositivo em Funcionamento

Com a finalização do nosso código, podemos utilizar o platformIO para fazer a compilação e transferência do código para o nosso dispositivo.

- Conectamos o dispositivos com usb ao computador;
- Depois basta utilizar a opção upload;
- Por ultimo podemos utilizar a função monitor para ver as resposta no serial.

Podemos ver abaixo uma imagem do PlatformIO dentro do VSCode.

<img src="https://www.imagemhost.com.br/images/2022/06/01/Screenshot-from-2022-06-01-11-40-36.png" width="300">

Uma vez que o código foi transferido com sucesso podemos ligar nosso dispositivo e, então, ele irá criar por 5 minutos uma rede WIFI que por padrão tem o nome de SAIOTDEVICE.


<img src="https://i.ibb.co/zP05cr4/20220517-100237.jpg" width="300">

Nessa rede colocaremos nome e senha da rede de WIFI que utilizaremos. E logo abaixo nossas credenciais do SAIOT.

E em poucos segundos teremos um dispositivo funcional conectado ao SAIOT.

<a id="ancora5"></a>
## Wishlist e Bugs

#WishList

- [X] Integração da biblioteca com o ESP8266;
- [X] Integração da biblioteca com o ESP32;
- [ ] Integração da fila de Eventos com a memoria Flash;
- [ ] Testes extensos biblioteca;
- [ ] Melhora da Performance da biblioteca;

#Bugs

Reportar qualquer bug aos contribuidores atuais

<a id="ancora6"></a>
## Contribuições e Agradecimentos

Versão atual: 1.7.0

main contribuitor: @Ewerton.Lopes

secondary contribuitor: @maycon.lima.118

past contribuitors:
