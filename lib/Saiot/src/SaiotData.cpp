#include "SaiotData.h"

sensorData::sensorData(String id,
                       String value,
                       String date): id(id),
                                     value(value),
                                     date(date)
{}

sensorData::~sensorData()
{}

String sensorData::getJson()
{
  char Json[128];
  StaticJsonDocument<128> doc;
  JsonObject root = doc.to<JsonObject>();
  root["id"] = id;
  root["value"] = value;
  //root["date"] = date;
  serializeJson(root,Json,128);
  return String(Json);
}

String sensorData::getTopic(String idDispositivo){return "measurements/" + idDispositivo;}

String sensorData::saveData()
{
  String Data;
  Data = "s,"+ id + "," + value + "," + date + "\n";
  return Data;
}

eventData::eventData(String id,
                     String description,
                     String date,
                     int wifiSignalLevel,
                     int bateryLevel,
                     Evtype criticality): id(id),
                                          description(description),
                                          date(date),
                                          wifiSignalLevel(wifiSignalLevel),
                                          bateryLevel(bateryLevel),
                                          criticality(criticality)
{}


String eventData::getJson()
{
  char Json[256];
  StaticJsonDocument<256> doc;
  JsonObject root = doc.to<JsonObject>();
  root["id"] = id;
  root["description"] = description;
  root["datetimeCreated"] = date;
  root["wifiSignalLevel"] = wifiSignalLevel;
  root["batteryLevel"] = bateryLevel;

  if(criticality == Evtype::alarm) root["criticality"] = "alarm";
  else if(criticality == Evtype::event) root["criticality"] = "event";
  else if(criticality == Evtype::normalization) root["criticality"] = "normalization";
  serializeJson(root,Json,256);
  
  return String(Json);
}

String eventData::getTopic(String idDispositivo){return "events/" + idDispositivo;};

Evtype eventData::getType() {return criticality;};

String eventData::getNotifyJson()
{
    char Json[128];
    StaticJsonDocument<128> doc;
    JsonObject root = doc.to<JsonObject>();
    root["eventId"] = id;

    if(criticality == Evtype::alarm) root["criticality"] = "alarm";
    else if(criticality == Evtype::event) root["criticality"] = "event";
    else if(criticality == Evtype::normalization) root["criticality"] = "normalization";
    serializeJson(root,Json,128);
    return String(Json);
}

String eventData::getNotifyTopic(String idDispositivo){return "notifications/" + idDispositivo;};

String eventData::saveData()
{
  String Data;
  if(criticality == Evtype::alarm) Data = "a,";
  else if(criticality == Evtype::event) Data = "e,";
  else if(criticality == Evtype::normalization) Data = "n,";
  
  Data = id + "," + description + "," + date + "," + String(wifiSignalLevel) + "," + String(bateryLevel) + "\n";
  return Data;
}