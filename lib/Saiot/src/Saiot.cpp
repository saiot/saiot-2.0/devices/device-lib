#include "Saiot.h"
 
/*!
 * @brief  class constructor
 * @param outPin analog pin connected to OUT
 */

#if defined(ESP8266)
#elif defined(ESP32)
void writeFile(fs::FS &fs, const char * path, const char * message){
    Serial.printf("Writing file: %s\n", path);

    File file = fs.open(path, FILE_WRITE);
    if(!file){
        Serial.println("Failed to open file for writing");
        return;
    }
    if(file.print(message)){
        Serial.println("File written");
    } else {
        Serial.println("Write failed");
    }
}

void deleteFile(fs::FS &fs, const char * path){
    Serial.printf("Deleting file: %s\n", path);
    if(fs.remove(path)){
        Serial.println("File deleted");
    } else {
        Serial.println("Delete failed");
    }
}


String readFile(fs::FS &fs, const char * path){
    Serial.printf("Reading file: %s\n", path);

    File file = fs.open(path);
    if(!file || file.isDirectory()){
        Serial.println("Failed to open file for reading");
        return "a";
    }
    String data = "";
    Serial.print("Read from file: ");
    while(file.available()){
        data += (char)file.read();
    }
    return data;
}
#endif

Saiot::Saiot(String ndeviceName,
             String ndeviceClass,
             String ndeviceDescription):deviceName(ndeviceName),
                                        deviceClass(ndeviceClass),
                                        deviceDescription(ndeviceDescription)
{
  // Procurando pelo id do dispositivo

  pubSubClient.setClient(espClient);
  pubSubClient.setBufferSize(512);
  pubSubClient.setServer(broker_url.c_str(), broker_port);
  pubSubClient.setCallback([this](char *topic, byte *payload, unsigned int length) { this->mqttCallback(topic, payload, length); });
}

/*!
 * @brief  class constructor with possibility to turn sensor ON/OFF
 * @param outPin analog pin connected to OUT
 * @param enablePin digital pin connected to EN, 
 *   this is used for turning the sensor ON/OFF
 */
Saiot::~Saiot()
{}

void Saiot::addActuator(Actuator &newActuator){actuators.push_back(&newActuator);}

void Saiot::addSensor(Sensor &newSensor){sensors.push_back(&newSensor);}

void Saiot::addEvent(Event &newEvent){events.push_back(&newEvent);}

void Saiot::setSSID(String nSSID){this->SSIDName=nSSID;}

void Saiot::setMaxbuffersize(int nmbuffer){this->MaxBufferSize = nmbuffer;}

void Saiot::setBrokerUrl(String nURL)
{
  this->broker_url = nURL;

  pubSubClient.setServer(broker_url.c_str(), broker_port);
}

void Saiot::setBrokerUrl(String nURL,int nport)
{
  this->broker_url = nURL;
  this->broker_port = nport;

  pubSubClient.setServer(broker_url.c_str(), broker_port);
}

void Saiot::setCredentials(String nEmail, String nPassword)
{
  this->email = nEmail;

  this->password = nPassword;
}

void Saiot::start()
{

  #if defined(ESP8266)
  if (LittleFS.begin())
  {
    /*
    LittleFS.format();
    */
    if(!checkAccount())
    {
      Serial.println("SPIFFS mounted...");
      id = "ESP8266ClientFailCheck";
    }
  }
  else
  {
    Serial.println("SPIFFS mount failed...");
    id = "ESP8266ClientFailMount";
  }
#elif defined(ESP32)
  if (SPIFFS.begin(true))
  {
    /*
    deleteFile(SPIFFS, "/e.txt");
    deleteFile(SPIFFS, "/s.txt");
    deleteFile(SPIFFS, "/i.txt");
    */
    if(!checkAccount())
    {
      Serial.println("SPIFFS mounted...");
      id = "ESP8266ClientFailCheck";
    }
  }
  else
  {
    Serial.println("SPIFFS mount failed...");
    id = "ESP8266ClientFailMount";
  }
#endif

  connectWifi();

  #if defined(ESP8266)
  configTime("<-03>3","10.7.227.97", "br.pool.ntp.org");
  #elif defined(ESP32)
  configTime(-10800,0,"10.7.227.97","br.pool.ntp.org");
  #endif

  delay(1000);

  Event turnon = Event(this->deviceName,"Dispositivo Ligado",0,0,Evtype::event,NULL,NULL);

  char actualTime[30];
  getFormattedtime(actualTime);
  eBuffer.push_back(turnon.getEventData(actualTime));

  connectMqtt();
}

boolean Saiot::loop()
{
  if(WiFi.status() == WL_CONNECTED && !hasValidId) hasValidId = saveAccount();

  if(!(lastStatus == WL_CONNECTED) && WiFi.status() == WL_CONNECTED)
  {
    Event conn = Event(this->deviceName,(char*)"Dispositivo Conectado a Internet",0,0,Evtype::event,NULL,NULL);

    char actualTime[30];
    getFormattedtime(actualTime);
    eBuffer.push_back(conn.getEventData(actualTime));
  }
  else if(lastStatus == WL_CONNECTED && !(WiFi.status() == WL_CONNECTED))
  {
    Event conn = Event(this->deviceName,(char*)"Dispositivo Desconectado a Internet",0,0,Evtype::event,NULL,NULL);

    char actualTime[30];
    getFormattedtime(actualTime);
    eBuffer.push_back(conn.getEventData(actualTime));
  }
  lastStatus = WiFi.status();

  wifiManager.process();

  eventProcess();
  sensorProcess();
  queueProcess();

  if(WiFi.status() == WL_CONNECTED && enableOTAStart)
  {
    if(otastart) handleOTA();
    else
    {
      enableOTA();
      otastart = true;
    }
  }
 
  if (!pubSubClient.connected()) reconnect();

  return pubSubClient.loop();
}

void Saiot::eventProcess()
{
  //Custom Events
  for(unsigned int i(0);i<events.size();i++)
  {
    if(events[i]->callCondition())
    {
      events[i]->callCallback();

      char actualTime[30];
      getFormattedtime(actualTime);
      eBuffer.push_back(events[i]->getEventData(actualTime));
    }
  }
}

void Saiot::sensorProcess()
{
  for (unsigned int i = 0; i < sensors.size(); i++)
  {
    unsigned long now = millis();
    boolean timeout = abs(float(now - (sensors[i]->getLastSendTime()))) >= (sensors[i]->getTimeout() * 1000);
    float oldValue = sensors[i]->getValue()[0].toFloat();
    sensors[i]->setValue(sensors[i]->callCallback());
    float newValue = sensors[i]->getValue()[0].toFloat();
    boolean deadband = false;
    

    String comptype(sensors[i]->getType());

    if (comptype.compareTo("number") == 0 && sensors[i]->getDeadband()>0)
    {
      deadband = abs(newValue - oldValue) >= abs(oldValue * sensors[i]->getDeadband())/100;      
    }

    if (timeout || deadband)
    {
      if(timeout) sensors[i]->setLastSendTime(now);

      char actualTime[30];
      getFormattedtime(actualTime);
      sBuffer.push_back(sensors[i]->getSensorData(actualTime));
    }
  }
}

void Saiot::queueProcess()
{
  if(WiFi.status() == WL_CONNECTED && pubSubClient.connected() && logged)
  {
    //if(dataSaved) readBuffer();
    for(itSbuffer = sBuffer.begin();itSbuffer!=sBuffer.end();itSbuffer++)
    {
      if(sendMessage(itSbuffer->getTopic(this->id).c_str(),
                     itSbuffer->getJson().c_str()))
      {
        //Serial.println(itSbuffer->getTopic(this->id));
        //Serial.println(itSbuffer->getJson());

        sBuffer.erase(itSbuffer--);
      }
    }

    for(itEbuffer = eBuffer.begin();itEbuffer!=eBuffer.end();itEbuffer++)
    {
      
      if(sendMessage(itEbuffer->getTopic(this->id).c_str(),
                     itEbuffer->getJson().c_str()))
      {
        //Serial.println(itEbuffer->getTopic(this->id));
        //Serial.println(itEbuffer->getJson());

        sendMessage(itEbuffer->getNotifyTopic(this->id).c_str(),
            itEbuffer->getNotifyJson().c_str());
        
        //Serial.println(itEbuffer->getNotifyTopic(this->id));
        //Serial.println(itEbuffer->getNotifyJson());

        eBuffer.erase(itEbuffer--);
      }
    }
  }
  else
  {
    //if(buffer.size() > MaxBufferSize - 5) saveBuffer();
  }
  
}

void Saiot::connectWifi()
{
  wifiManager.addParameter(&custom_email);
  wifiManager.addParameter(&custom_senha);

  WiFi.mode(WIFI_STA);

  wifiManager.setConfigPortalBlocking(false);
  wifiManager.setConfigPortalTimeout(300);

  wifiManager.autoConnect(SSIDName.c_str())?Serial.println("Connectado ao WIFI"):Serial.println("Portal ativado");

  wifiManager.startConfigPortal(SSIDName.c_str());
}

bool Saiot::saveAccount()
{
    File any;

    strcpy(emailr, custom_email.getValue());
    email = emailr;

    if(email.length()<5)
    {
      email = "";
      return false;
    }

    File any2;
    strcpy(senhar, custom_senha.getValue());
    password = senhar;

    #if defined(ESP8266)
      any = LittleFS.open("e.txt","w");
      any2 = LittleFS.open("s.txt","w");
      if(!(any && any2)) return false;
      any.print(email);
      any2.print(password);
    #elif defined(ESP32)
      writeFile(SPIFFS, "/e.txt", email.c_str());
      writeFile(SPIFFS, "/s.txt", password.c_str());
    #endif

    firsttime = true;
    return true;
}

bool Saiot::checkAccount()
{
  #if defined(ESP8266)
    File file1 = LittleFS.open("i.txt", "r");
    File file2 = LittleFS.open("e.txt", "r");
    File file3 = LittleFS.open("s.txt", "r");
    if (file1 || file2 || file3)
    {
      hasValidId = true;
      hasValidId2 = true;

      while (file1.available()) id += (char)file1.read();
      file1.close();


      while (file2.available()) email += (char)file2.read();
      file2.close();


      while (file3.available()) password += (char)file3.read();
      file3.close();

      return true;

    }

  #elif defined(ESP32)
  String ler1 = readFile(SPIFFS, "/i.txt");
  String ler2 = readFile(SPIFFS, "/e.txt");
  String ler3 = readFile(SPIFFS, "/s.txt");


  if (!(ler1 == "a") || !(ler2 == "a") || !(ler3 == "a"))
  {
    this->id = ler1;
    this->email = ler2;
    this->password = ler3;
    
    hasValidId = true;
    hasValidId2 = true;

    return true;
  }
  #endif

  return false;
}

void Saiot::connectMqtt()
{
  pubSubClient.disconnect();

  do
  {
    //Serial.println("Tentando conectar com o broker...");
    pubSubClient.connect(id.c_str(), email.c_str(), password.c_str());
  } while (false);

  String idMsg(this->id + "/message");
  String idConfig(this->id + "/config");
  String idAct(this->id + "/act");

  subscribe(idMsg.c_str());
  subscribe(idConfig.c_str());
  subscribe(idAct.c_str());

  //Serial.println("Conectado no broker!");
}

void Saiot::reconnect()
{
  if (!pubSubClient.connected())
  {
    //Serial.println("Conexão perdida com o broker, tentando se reconectar...");

    if (pubSubClient.connect(id.c_str(), email.c_str(), password.c_str()))
    {
      String idMsg(this->id + "/message");
      String idConfig(this->id + "/config");
      String idAct(this->id + "/act");

      //Serial.println("Conectado no broker!");

      subscribe(idMsg.c_str());
      subscribe(idConfig.c_str());
      subscribe(idAct.c_str());

    }
    else
    {
      if(firsttime)
      {
        #if defined(ESP8266)
          LittleFS.end();
          LittleFS.format();
        #elif defined(ESP32)
          deleteFile(SPIFFS, "/e.txt");
          deleteFile(SPIFFS, "/s.txt");
          deleteFile(SPIFFS, "/i.txt");
          SPIFFS.end();
        #endif     
        ESP.restart();
      }

      //Serial.print("Falha ao se conectar ao broker, rc=");
      //Serial.print(pubSubClient.state());
      //Serial.println(" tentando novamente em 5 segundos");
    }
  }
}

String Saiot::json()
{
  String json = "";

  json += "{\"id\":\"";
  json += id;

  json += "\",\"email\":\"";
  json += email;

  json += "\",\"name\":\"";
  json += deviceName;

  json += "\",\"class\":\"";
  json += deviceClass;

  json += "\",\"description\":\"";
  json += deviceDescription;

  json += "\",\"sensors\":{";

  for (unsigned int i = 0; i < sensors.size(); i++)
  {
    json += sensors[i]->json();
    if (i != sensors.size() - 1)
    {
      json += ",";
    }
  }

  json += "}";

  json += ",\"actuators\":{";

  for (unsigned int i = 0; i < actuators.size(); i++)
  {
    json += actuators[i]->json();
    if (i != actuators.size() - 1)
    {
      json += ",";
    }
  }

  json += "}";

  json += "}";

  return json;
}

void Saiot::subscribe(const char *topic)
{
  boolean subscribe = 0;
  do
  {
    //Serial.print("Tentando se inscrever no topico ");
    //Serial.println(topic);

    subscribe = pubSubClient.subscribe(topic);
    if (!subscribe)
    {
      //Serial.println((String) "Falha ao se inscrever no tópico " + topic + ", tentando de novo em 5 segundos...");
      delay(5000);
    }

  } while (pubSubClient.connected() && !subscribe);

  if (pubSubClient.connected() && subscribe)
  {
    //Serial.println((String) "Inscrito com sucesso no topico " + topic);
  }
}

boolean Saiot::sendMessage(const char *topic, const char *payload)
{
  //Serial.print("Envio no topico ");
  //Serial.print(topic);
  //Serial.print(" com payload ");
  //Serial.println(payload);
  boolean publish = pubSubClient.publish(topic, payload);;

  return publish;
}

void Saiot::mqttCallback(char *topic, byte *payload, unsigned int length)
{
  // Parseando payload
  String parsedPayload = "";
  for (unsigned int i = 0; i < length; i++)
  {
    parsedPayload += (char)payload[i];
  }

  //Serial.println((String) "Mensagem recebida no tópico '" + topic + "' com payload '" + parsedPayload + "'");

  //Checando se dispositivo ja possui um id valido
  if (!hasValidId2)
  {
    //Serial.println("Recebido um novo id, reconectando...");
    id = parsedPayload;
    hasValidId2 = true;
    
    #if defined(ESP8266)
      File file = LittleFS.open("i.txt", "w");
      if(file) if(file.print(id));
      file.close();
    #elif defined(ESP32)
      writeFile(SPIFFS, "/i.txt", id.c_str());
    #endif
    connectMqtt();
  }
  else
  {
    // Tratativa dos tópicos
    if (String(topic).compareTo(id + "/message") == 0)
    {
      if (parsedPayload.compareTo("Cadastre-se") == 0)
      {
        logged = false;
        // Serial.println("Enviando json de configuração");
        sendMessage("register-device", json().c_str());
      }
      else if (parsedPayload.compareTo("Aprovado") == 0)
      {
        logged = false;
        if(firsttime)
        {
          firsttime = false;
        }
        // Serial.println("Dispositivo aprovado!");
        connectMqtt();
      }
      else if (parsedPayload.compareTo("Logado") == 0)
      {
        logged = true;
        // Serial.println("Dispositivo logado!");
      }
      else if (parsedPayload.compareTo("Recusado") == 0)
      {
        logged = false;
        // Serial.println("Dispositivo recusado!");
      }
      else if (parsedPayload.compareTo("Deletado") == 0)
      {
        #if defined(ESP8266)
          LittleFS.end();
          LittleFS.format();
        #elif defined(ESP32)
          deleteFile(SPIFFS, "/e.txt");
          deleteFile(SPIFFS, "/s.txt");
          deleteFile(SPIFFS, "/i.txt");
          SPIFFS.end();
        #endif     
        ESP.restart();
        // Serial.println("Dispositivo deletado!");
      }
      else if (parsedPayload.compareTo("Aguardando aprovação") == 0)
      {
        logged = false;
        // Serial.println("Dispositivo aguardando aprovação...");
      }
    }
    else if (String(topic).compareTo(id + "/config") == 0)
    {
      DynamicJsonDocument parsedJsonPayload(512);
      DeserializationError error = deserializeJson(parsedJsonPayload, parsedPayload);

      if (error)
      {
        // Serial.println(F("deserializeJson() failed: "));
        // Serial.println(error.f_str());
        return;
      }

    const char* deviceName_n = parsedJsonPayload["name"];
    deviceName = deviceName_n;
    //Serial.println(deviceName);
    const char* deviceClass_n = parsedJsonPayload["class"];
    deviceClass = deviceClass_n;
    //Serial.println(deviceClass);
    const char* deviceDescription_n = parsedJsonPayload["description"];
    deviceDescription = deviceDescription_n;
    //Serial.println(deviceDescription);

    for (unsigned int i = 0; i < sensors.size(); i++)
    {
    float timeout_n = parsedJsonPayload["sensors"][sensors[i]->getId()]["timeout"];
    sensors[i]->setTimeout(timeout_n);
    int deadband_n = parsedJsonPayload["sensors"][sensors[i]->getId()]["deadband"];
    sensors[i]->setDeadband(deadband_n);

    //Serial.println(timeout_n);
    //Serial.println(deadband_n);
    }

    }
    else if (String(topic).compareTo(id + "/act") == 0)
    { 
      DynamicJsonDocument parsedJsonPayload(200);
      DeserializationError error = deserializeJson(parsedJsonPayload, parsedPayload);


      if (error)
      {
        // Serial.println(F("deserializeJson() failed: "));
        // Serial.println(error.f_str());
        return;
      }

      const char *actuatorId = parsedJsonPayload["id"];
      // Serial.println((String) "Recebido ação para atuador com id " + actuatorId);

      //char **actuatorParameters = parsedJsonPayload["parameters"];
      JsonArray array = parsedJsonPayload["parameters"].as<JsonArray>();
      std::vector<String> actuatorParameters;

      for (unsigned int i = 0; i < array.size(); i++)
      {
        actuatorParameters.push_back(array[i].as<String>());
      }

      for (unsigned int i = 0; i < actuators.size(); i++)
      {
        if (actuators[i]->getId().compareTo(actuatorId) == 0)
        {
          std::vector<String> ar = actuators[i]->callCallback(actuatorParameters);
          String result("");
          //"\""
          for(unsigned int i =0; i<ar.size(); i++)
          {
          result += ar[i];
          if(i != ar.size()-1) result += ",";   
          }
          result += "";
          String actReturnTopic = (String) "act-return/" + id;
          String newPayload = (String) "{\"id\":\"" + actuatorId + "\",\"value\":" + result + "}";

          sendMessage(actReturnTopic.c_str(), newPayload.c_str());
        }
      }
    }
  }
}

void Saiot::actState(int actnumb, String newState)
{
  String actReturnTopic = "act-return/" + id;

  String comp(actuators[actnumb]->getId());

  char Json[128];
  StaticJsonDocument<128> doc;
  JsonObject root = doc.to<JsonObject>();
  root["id"] = comp;
  root["value"] = newState;
  serializeJson(root,Json,128);

  sendMessage(actReturnTopic.c_str(), Json);
}

void Saiot::getFormattedtime(char* formTime)
{
  //unsigned long epochTime = timeClient.getEpochTime();
  struct tm *tmstruct;
  time_t tnow = time(nullptr);

  tmstruct = localtime(&tnow);;

  strftime(formTime, 30, "%F %T", tmstruct);

  //Serial.println(formTime);

}

/*bool Saiot::saveBuffer()
{
  File file = LittleFS.open("/Queue/" + String(dataSaved),"w");
  String upper = String(buffer.size()) + "\n";
  
  file.write(upper.c_str());

  for(it_buffer = buffer.begin();it_buffer!=buffer.end();it_buffer++)
  {
  if(!file.write(it_buffer->saveData().c_str())) return false;
  }

  buffer.clear();
  file.close();

  dataSaved++;

  return true;
}*/

/*
bool Saiot::readBuffer()
{
  File file = LittleFS.open("/Queue/" + String(dataSaved - 1),"r");
  String number = file.readStringUntil('\n');

  for(int i(0);i<number.toInt();i++)
  {
    String type = file.readStringUntil(',');
    if(type=="s")
    {
      String id = file.readStringUntil(',');
      String value = file.readStringUntil(',');
      String date = file.readStringUntil('\n');
      sensorData senData(id,value,date);
      buffer.push_back(senData);
    }
    else if(type == "a" || type == "e" || type == "n")
    {
      String oid = file.readStringUntil(',');
      String odescription = file.readStringUntil(',');
      String odate = file.readStringUntil(',');
      String owifi = file.readStringUntil(',');
      String obattery = file.readStringUntil('\n');
      Evtype oevent = Evtype::other;
      if(type == "a") oevent = Evtype::alarm;
      else if(type == "e") oevent = Evtype::event;
      else if(type == "n") oevent = Evtype::normalization;

      eventData evData(oid,odescription,odate,owifi.toInt(),obattery.toInt(),oevent);
      buffer.push_back(evData);
    }
  }

  dataSaved--;
  return true;
}
*/

void Saiot::enableOTA()
{
  ArduinoOTA.onStart([]() {
    Serial.println("Iniciando Server OTA");
  });
  ArduinoOTA.onEnd([]() {
    Serial.println("Fechando Server OTA!");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progresso: %u%%r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Erro [%u]: ", error);
    if (error == OTA_AUTH_ERROR) Serial.println("Autenticacao Falhou");
    else if (error == OTA_BEGIN_ERROR) Serial.println("Falha no Inicio");
    else if (error == OTA_CONNECT_ERROR) Serial.println("Falha na Conexao");
    else if (error == OTA_RECEIVE_ERROR) Serial.println("Falha na Recepcao");
    else if (error == OTA_END_ERROR) Serial.println("Falha no Fim");
  });
  ArduinoOTA.begin();
  // Serial.println("OTA Funcionando");
  // Serial.print("Endereco IP: ");
  Serial.println(WiFi.localIP());
}

void Saiot::handleOTA()
{
  ArduinoOTA.handle();
}

void Saiot::disableOTA()
{
  enableOTAStart = false;
}
